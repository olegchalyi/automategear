﻿using System;

namespace AutomateGear
{
    internal class Program
    {
        const string Forward = "Forward";
        const string ForwardOrReverseSelection = "Will you drive forward or reverse? Select F or R: ";
        const byte countAttemption = 3;

        // Main method
        static void Main(string[] args)
        {//оголошення констант
            Console.WriteLine("Hello friends! Wellcome to us!");
            const int neutralGear = 0;
            const int firstGear = 1;
            const int secondGear = 2;
            const int thirdGear = 3;
            const int fourthGear = 4;
            const int fifthGear = 5;
            const int sixthGear = 6;
            const int reverseGear = -1;
            int routateEngine = 0;
            int? velocity = 0;//оголошення змінних
            int gear = neutralGear;
            string driveDirection = string.Empty;

            Console.Write(ForwardOrReverseSelection);
            driveDirection = ValidationInputStringValue(Console.ReadLine());
            //умовні оператори ;)
            if (driveDirection != null)
            {
                Console.WriteLine("Input your velocity: ");
                velocity = ValidationInputValue(velocity);
            }

            if (driveDirection == "F" && velocity != null)
            {
                driveDirection = Forward;

                gear = (0 < velocity && velocity < 21 && routateEngine < 2500) ? firstGear
                 : (20 < velocity && velocity < 41 && routateEngine < 2500) ? secondGear
                 : (40 < velocity && velocity < 61 && routateEngine < 2500) ? thirdGear
                 : (60 < velocity && velocity < 81 && routateEngine < 2500) ? fourthGear
                 : (80 < velocity && velocity < 101 && routateEngine < 2500) ? fifthGear
                 : (velocity > 101) ? sixthGear : neutralGear;

                DisplayInTheTable(velocity, gear, driveDirection);
            }
            else if (driveDirection == "R" && velocity != null)
            {
                driveDirection = "Reverse";

                gear = reverseGear;
                byte maxSpeedForReverseDirection = 40;
                if (velocity > maxSpeedForReverseDirection)
                {
                    Console.Beep();
                    Console.WriteLine("Error speed value! It is not for reverse!!!");
                    velocity = null;
                }

                DisplayInTheTable(velocity, gear, driveDirection);
            }
        }
        //метод
        static int? ValidationInputValue(int? velocity)
        {

            for (int i = 0; i < countAttemption; i++)
            {
                bool isInt = int.TryParse(Console.ReadLine(), out int newVelocity);
                velocity = newVelocity;

                if (!isInt)
                {
                    Console.WriteLine("Pass number value");
                    continue;
                }
                if (i == 2)
                {
                    Console.WriteLine("Your quantity attemption is over.");
                    Console.WriteLine("Try again.");
                    Console.WriteLine("Avoid such cases.");
                    return null;
                }
                break;//this operator break the for loop
            }

            return velocity;
        }

        static string? ValidationInputStringValue(string input)
        {
            byte maxNumberAttemptions = 2;

            for (int i = 0; i < countAttemption; i++)
            {
                if (input != "F" && input != "R")
                {
                    Console.WriteLine("Please select F or R!");
                    input = Console.ReadLine();
                }
                else if (input == "F" || input == "R")
                {
                    return input;
                }
                if(i == maxNumberAttemptions)
                {
                    Console.WriteLine("Your quantity attemption is over.");
                    return null;
                }
            }

            return input;
        }

        static void DisplayInTheTable(int? velocity, int gear, string driveDirection)
        {
            Console.WriteLine(new string('-', 49));
            Console.WriteLine("|{0,-15}|{1,-15}|{2,-15}|", "Velocity", "Gear", "Direction");
            Console.Write(new string('-', 16));
            Console.Write('+');
            Console.Write(new string('-', 15));
            Console.Write('+');
            Console.WriteLine(new string('-', 16));
            Console.WriteLine("|{0,-15}|{1,-15}|{2,-15}|", velocity, gear, driveDirection);
            Console.WriteLine(new string('-', 49));
        }
    }
}
